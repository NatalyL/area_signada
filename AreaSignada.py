# Area Signada

# Pedimos los datos de los puntos de coordenadas
x1 = int(input("Escriba el punto x de la coordenada A: "))
y1 = int(input("Escriba el punto y de la coordenada A: "))

x2 = int(input("Escriba el punto x de la coordenada B: "))
y2 = int(input("Escriba el punto y de la coordenada B: "))

x3 = int(input("Escriba el punto x de la coordenada C: "))
y3 = int(input("Escriba el punto y de la coordenada C: "))

# Nos calcula el area signada de los punto A, B y C con esa orientación.
# Siendo positiva si la orientación sigue el sentido antihorario , negativa en caso contrario y cero si los puntos están alineados.
def sarea(A,B,C):
    return (1/2)*((B[0]-A[0])*(C[1]-A[1])-(C[0]-A[0])*(B[1]-A[1]))

# Condiciones
if sarea([x1, y1], [x2, y2], [x3, y3]) > 0:
    print('-----------------------------')
    print('Como ', sarea([x1, y1], [x2, y2], [x3, y3]), '> 0, su giro es antihorario')
else:
    print('-----------------------------')
    print('Como ', sarea([x1, y1], [x2, y2], [x3, y3]), '< 0, su giro es horario')